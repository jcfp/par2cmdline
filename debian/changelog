par2cmdline (0.8.1-3) unstable; urgency=medium

  * Watch: switch to github api.
  * Control: bump std-ver to 4.6.1 (from 4.6.0; no changes).
  * Control: remove obsolete automake1.4 from build-conflicts.

 -- Jeroen Ploemen <jcfp@debian.org>  Sun, 04 Dec 2022 21:42:51 +0000

par2cmdline (0.8.1-2) unstable; urgency=low

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Jeroen Ploemen ]
  * Use my debian.org email address for maintainer and copyright.
  * Watch: bump version to 4 (from 3; no further changes).
  * Control: set Rules-Requires-Root: no.
  * Control: bump Standards-Version to 4.6.0 (from 4.5.0; no further
    changes).
  * Control: bump compat level to 13 (from 12; no further changes).
  * Copyright: bump packaging year.

 -- Jeroen Ploemen <jcfp@debian.org>  Thu, 02 Dec 2021 14:18:48 +0000

par2cmdline (0.8.1-1) unstable; urgency=medium

  * New upstream release.
  * Patches:
    + refresh 01 to remove fuzz.
    + add 02_fix_syntax_error_in_tests.
    + add 03_backport_upstream_pr_144, fixes unit tests failure on
      big-endian archs.
  * Rules: drop redundant '--with autoreconf' from dh sequencer.
  * Bump Standards-Version to 4.5.0 (from 4.4.1; no further changes).
  * Copyright: refresh upstream years, add new author.

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Tue, 11 Feb 2020 18:24:35 +0000

par2cmdline (0.8.0-2) unstable; urgency=medium

  * Control: switch Vcs fields to salsa.d.o.
  * Compat:
    + delete compat file, build-depend on debhelper-compat instead.
    + bump compat level to 12.
  * Patches: add 01_test_tar_extract_issue.
  * Tests: add upstream-tests as autopkgtest for debian-ci.
  * Changelog: remove trailing whitespace.
  * Copyright:
    + use secure Format URI.
    + bump year for packaging.
  * Bump Standards-Version to 4.4.1 (from 4.1.3; no further changes).

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Wed, 08 Jan 2020 11:06:02 +0000

par2cmdline (0.8.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.1.3 (from 4.1.0; no further changes).

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Wed, 03 Jan 2018 13:43:37 +0000

par2cmdline (0.7.4-1) unstable; urgency=medium

  * New upstream release.
  * Control:
    + mark package multi-arch: foreign. (Closes: #865651)
    + remove build-dep on dh-autoreconf, redundant with compat 10.
  * Bump Standards-Version to 4.1.0 (from 3.9.8; no further changes).
  * Compat: bump to 10 (from 9) and set debhelper version accordingly.
  * Rules: drop override for dh_installchangelogs, file is found
    automatically now.

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Thu, 21 Sep 2017 19:31:33 +0000

par2cmdline (0.7.2-1) unstable; urgency=medium

  * New upstream release:
    + adds multithreading support. (Closes: #466730) (LP: #197852)
  * Watch:
    + adjust file matching regex to match upstream changes.
    + add verification of upstream cryptographic signature.
  * Patches: drop all, merged upstream.
  * Rules: remove dh_clean override, no longer needed.
  * Copyright: bump years for upstream and packaging.

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Wed, 21 Jun 2017 18:53:23 +0000

par2cmdline (0.6.14-2) unstable; urgency=medium

  * Control: switch git url to https.
  * Rules: enable all hardening options.
  * Patches:
    + add 02: restore the examples section from the previous manpage,
      written by Andres Salomon. (Closes: #827720)
    + add 03: avoid unconditional use of PATH_MAX which isn't defined on
      hurd. Fixes build failure on that platform.
  * Bump Standards-Version to 3.9.8 (from 3.9.6, no further changes).

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Wed, 11 Jan 2017 16:37:43 +0000

par2cmdline (0.6.14-1) unstable; urgency=medium

  * New upstream release.
  * Add patch 01: use /dev/urandom instead of /dev/random in test 20
    to prevent lengthy delays during build.

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Mon, 10 Aug 2015 12:39:45 +0000

par2cmdline (0.6.13-1) unstable; urgency=medium

  * New upstream release:
    + Adds argument of option -b to the manpage. (Closes: #782980)
    + Fixes repair error with nested directories. (Closes: #781129)
    + Fixes failure to find data blocks beyond the point of damage
      after removal of a single byte. (Closes: #785127)
  * Update homepage, watchfile to match renamed github repository.
  * Bump copyright years.

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Thu, 28 May 2015 20:12:28 +0000

par2cmdline (0.6.11-1) unstable; urgency=medium

  * New upstream release:
    + Fixes a regression causing failure to locate misnamed files
      during repair. (Closes: #769820)
  * Bump standards-version to 3.9.6 (no changes needed).

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Mon, 17 Nov 2014 19:17:03 +0000

par2cmdline (0.6.10-1) unstable; urgency=medium

  * New upstream release:
    + Fixes crash during verification. (Closes: #759997)

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Wed, 10 Sep 2014 03:28:10 +0400

par2cmdline (0.6.8-1) unstable; urgency=medium

  * New upstream release.

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Fri, 08 Aug 2014 22:56:16 +0200

par2cmdline (0.6.7-1) unstable; urgency=medium

  * New upstream release:
    + Adds support for specifying absolute size of recovery data.
      (Closes: #336102) (LP: #535309)
    + Fixes error where repair would sometimes read beyond EOF.
      (Closes: #415984)
  * Install upstream man page.

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Mon, 12 May 2014 22:29:53 +0200

par2cmdline (0.6.5-1) unstable; urgency=low

  * New upstream release:
    + Switch sources to Github-based fork; the original project
      (on Sourceforge) has been inactive for almost a decade.
    + Remove all patches: included upstream.
    + Fixes selection of inefficient block size (Closes: #655334).
  * New maintainer (Closes: #673225).
  * Switch to 3.0 (quilt) source format.
  * Set priority to optional.
  * Bump standards-version to 3.9.5 (from 3.8.3).
  * Switch debian/rules to dh sequencer with autoreconf.
  * Update build-deps:
    + Add dh-autoreconf.
    + Remove cdbs, autotools-dev, automake1.9 (Closes: #724427).
    + Set debhelper version and compat level to 9.
  * Docs:
    + Replace PORTING and ROADMAP files with README.
    + Install upstream changelog.
  * Man page:
    + Fix numerous instances of hyphens used as minus signs.
    + Install links to a single manpage instead of keeping around
      multiple copies in the packaging.
  * Refresh copyright:
    + List new upstream developers.
    + Add myself as a copyright holder for the packaging.
    + Switch to machine-readable format.
  * Add Vcs-* fields.

 -- JCF Ploemen (jcfp) <linux@jcf.pm>  Fri, 07 Mar 2014 15:24:21 +0100

par2cmdline (0.4-11) unstable; urgency=low

  * debian/rules: Preserve updated aclocal.m4.  Fixes FTBFS.  Closes: #544645.

 -- Bart Martens <bartm@debian.org>  Wed, 02 Sep 2009 12:59:45 +0200

par2cmdline (0.4-10) unstable; urgency=low

  * debian/control, debian/rules: No longer use automake1.8.  Closes: #473346.
    Thanks to Eric Dorland <eric@debian.org>.

 -- Bart Martens <bartm@debian.org>  Sun, 30 Mar 2008 07:13:58 +0200

par2cmdline (0.4-9) unstable; urgency=low

  * New maintainer.  Closes: #396621.
  * debian/compat, debian/control: Use debhelper 5.
  * debian/copyright: Updated.
  * debian/patches/000_unidentified.diff: Moved various existing changes to
    this patch file.
  * debian/rules: Updated to keep generated changes out of the .diff.gz.
  * debian/watch: Updated.
  * debian/patches/004_kfreebsd.diff: Added.  Closes: #415106.  Patch by Cyril
    Brulebois <cyril.brulebois@enst-bretagne.fr>, thanks.

 -- Bart Martens <bartm@knars.be>  Sat, 24 Mar 2007 13:08:11 +0100

par2cmdline (0.4-8) unstable; urgency=low

  * Allow operation on block devices (closes: #362558)
    - (Thanks to  Peter Cordes <peter@cordes.ca>)
  * Now using Standards-Version 3.7.2.0 (no changes necessary)

 -- Wesley J. Landaker <wjl@icecavern.net>  Sun,  4 Jun 2006 13:10:27 -0600

par2cmdline (0.4-7) unstable; urgency=low

  * Patch to fix G++ 4.1 issue (closes: #356767)
    - (Thanks to Ben Hutchings <ben@decadentplace.org.uk>)

 -- Wesley J. Landaker <wjl@icecavern.net>  Sat, 25 Mar 2006 18:57:18 -0700

par2cmdline (0.4-6) unstable; urgency=low

  * Applied patch to fix crash in quiet modes (closes: #299658)
    - (Thanks to Stelios Bounanos <sb-bugs@enotty.net>)
  * Added a few more relevant files to debian/docs
  * Added debian/watch to scan for upstream versions

 -- Wesley J. Landaker <wjl@icecavern.net>  Thu, 29 Sep 2005 07:51:05 -0600

par2cmdline (0.4-5) unstable; urgency=low

  * Whoops, *actually* updated mainter info this time. =)
  * Added debian/svn-deblayout (for use with svn-buildpackage)

 -- Wesley J. Landaker <wjl@icecavern.net>  Sun, 18 Sep 2005 17:24:55 -0600

par2cmdline (0.4-4) unstable; urgency=low

  * Adopting package, new maintainer (closes: #321457)
  * Updated to Standards-Version 3.6.2.0

 -- Wesley J. Landaker <wjl@icecavern.net>  Sun, 18 Sep 2005 16:45:25 -0600

par2cmdline (0.4-3) unstable; urgency=low

  * Update my email address.

 -- Andres Salomon <dilinger@debian.org>  Fri, 05 Aug 2005 12:13:25 -0400

par2cmdline (0.4-2) unstable; urgency=low

  * Fix gcc-4.0 build failures; thanks to Andreas Jochens (closes: #287904).

 -- Andres Salomon <dilinger@voxel.net>  Thu, 30 Dec 2004 21:21:01 -0500

par2cmdline (0.4-1) unstable; urgency=low

  * New upstream release.
  * Update package to use automake1.8 instead of 1.7.
  * Add manpages for par2 and symlinks (closes: #246130).

 -- Andres Salomon <dilinger@voxel.net>  Thu, 27 May 2004 14:19:30 -0400

par2cmdline (0.3-1) unstable; urgency=low

  * Initial Release.  (Closes: #201125)

 -- Andres Salomon <dilinger@voxel.net>  Wed, 14 Jan 2004 22:47:56 -0500
